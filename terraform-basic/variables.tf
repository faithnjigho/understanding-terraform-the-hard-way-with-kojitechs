
variable "cidr_block" {
    default  = "10.0.0.0/16"
    type =  string
    description = "classless interdomain range for IPs"
    
}
variable "ami_id" {
    default = "ami-09d3b3274b6c5d4aa"
    type = string
    description = "ami_id"
  
}